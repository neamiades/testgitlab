using System;

namespace dotnetcore
{
    public class Car
    {
        public string Speed { get; set; }
        public string Agile { get; set; }
        public User User { get; set; }
    }

    public class WarCar: Car
    {
        public string Weapon { get; set; }
    }

    public class CatCar: Car
    {
        public string Weapon { get; set; }
        public Cat Cat { get; set; }
    }

    public class FatCar: Car
    {
        public string Weapon { get; set; }
        public Cat Cat { get; set; }
    }

    public class PCar: Car
    {
        public string Weapon { get; set; }
        public Cat Cat { get; set; }
    }
}
