using System;

namespace dotnetcore
{
    public class Cat
    {
        public string Name { get; set; }
        public string Food { get; set; }
    }
}
