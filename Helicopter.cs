using System;

namespace dotnetcore
{
    public class Helicopter
    {
        public string Speed { get; set; }
        public string Pilot { get; set; }
    }
}
