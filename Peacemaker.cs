using System;

namespace dotnetcore
{
    public class Peacemaker
    {
        public string Name { get; set; }
        public string Weapon { get; set; }
        public string Country { get; set; }

        public bool MakePeace() { return Country == "USA"; }
        public bool Sacrifice() { return Weapon == "Granade"; }
    }
}
