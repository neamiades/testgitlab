using System;

namespace dotnetcore
{
    public class Dog
    {
        public string Name { get; set; }
        public string Food { get; set; }
        public User User { get; set; }
    }
    public class Spaniel: Dog
    {
    }
}
