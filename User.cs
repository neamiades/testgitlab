using System;

namespace dotnetcore
{
    public class User
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Cars[] Cars { get; set; }
    }
}
