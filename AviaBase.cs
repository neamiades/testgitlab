using System;

namespace dotnetcore
{
    public class Aviabase
    {
        public Helicopter[] Helicopters { get; set; }
        public WarCar[] WarCars { get; set; }
        public string Name { get; set; }
    }
}
